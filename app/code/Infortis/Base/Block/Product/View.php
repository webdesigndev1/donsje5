<?php
/**
 * Page header block
 */

namespace Infortis\Base\Block\Product;

use Infortis\Base\Helper\Data as HelperData;
use Infortis\Base\Helper\Template\Catalog\Product\View as HelperTemplateProductView;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManagerInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\ConfigurableProduct\Api\LinkManagementInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;

class View extends \Magento\Framework\View\Element\Template
{
    /**
     * Theme helper
     *
     * @var HelperData
     */
    protected $theme;

    /**
     * Product view helper
     *
     * @var HelperTemplateProductView
     */
    protected $helperProductView;
    protected $storeManager;
    protected $stockRegistry;
    protected $linkManagement;
    protected $priceCurrency;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @param Context $context
     * @param HelperData $helperData
     * @param HelperTemplateProductView $helperTemplateProductView
     * @param array $data
     */
    public function __construct(
        Context $context,
        HelperData $helperData,
        HelperTemplateProductView $helperTemplateProductView,
        Registry $registry,
        StoreManagerInterface $storeManager,
        StockRegistryInterface $stockRegistry,
        LinkManagementInterface $linkManagement,
        PriceCurrencyInterface $priceCurrency,
        array $data = []
    ) {
        $this->theme = $helperData;
        $this->helperProductView = $helperTemplateProductView;
        $this->registry = $registry;
        $this->storeManager = $storeManager;
        $this->stockRegistry = $stockRegistry;
        $this->linkManagement = $linkManagement;
        $this->priceCurrency = $priceCurrency;

        parent::__construct($context, $data);
    }

   /**
     * Get helper
     *
     * @return HelperData
     */
    public function getHelperTheme()
    {
        return $this->theme;
    }

   /**
     * Get helper
     *
     * @return HelperTemplateProductView
     */
    public function getHelperProductView()
    {
        return $this->helperProductView;
    }

    /**
     * Retrieve currently viewed product object
     *
     * @return \Magento\Catalog\Model\Product
     */
    public function getProduct()
    {
        return $this->registry->registry('product');
    }

    // Get code for current store
    public function getStoreCode()
    {
        return $this->storeManager->getStore()->getCode();
    }

    public function getStoreId()
    {
        return $this->storeManager->getStore()->getId();
    }

    /**
     * Get the product stock data and methods.
     *
     * @return \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    public function getStockRegistry()
    {
        return $this->stockRegistry;
    }

    /**
     * Get the children of the current product.
     *
     * @return array
     */
    public function getProductChildren()
    {
        return $this->linkManagement->getChildren($this->getProduct()->getSku());
    }

    public function getMSRP($product)
    {
        $price = $product->getData('msrp');
        return $this->getFormattedPrice($price);
    }

    public function getFormattedPrice($amount)
    {
        return $this->priceCurrency->convertAndFormat($amount);
    }

}
