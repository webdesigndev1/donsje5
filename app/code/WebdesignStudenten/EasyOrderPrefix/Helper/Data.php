<?php
namespace WebdesignStudenten\EasyOrderPrefix\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{

	const ORDER_PREFIX = 'easyOrderPrefix/general/order_prefix';
    
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $_scopeConfig;
    
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        $this->_scopeConfig = $context->getScopeConfig();
        parent::__construct($context);
    }

	public function getOrderNumberPrefix() {
        return $this->_scopeConfig->getValue(self::ORDER_PREFIX, ScopeInterface::SCOPE_STORES);
	}

}