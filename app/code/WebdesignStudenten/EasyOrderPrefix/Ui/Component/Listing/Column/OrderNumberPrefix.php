<?php
/**
 * mc-magento2 Magento Component
 *
 * @category Ebizmarts
 * @package mc-magento2
 * @author Ebizmarts Team <info@ebizmarts.com>
 * @copyright Ebizmarts (http://ebizmarts.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @date: 3/15/17 1:23 AM
 * @file: Monkey.php
 */
namespace WebdesignStudenten\EasyOrderPrefix\Ui\Component\Listing\Column;

use \Magento\Framework\View\Element\UiComponent\ContextInterface;
use \Magento\Framework\View\Element\UiComponentFactory;
use \Magento\Ui\Component\Listing\Columns\Column;

class OrderNumberPrefix extends Column
{
    /**
     * @var \WebdesignStudenten\EasyOrderPrefix\Helper\Data
     */
    protected $_helper;

    /**
     * Monkey constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param \WebdesignStudenten\EasyOrderPrefix\Helper\Data $helper
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \WebdesignStudenten\EasyOrderPrefix\Helper\Data $helper,
        array $components = [],
        array $data = []
    ) {
    
        $this->_helper = $helper;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $len = strlen($this->_helper->getOrderNumberPrefix());
                if (!empty($item['order_increment_id'])) {
                    $prefix = substr($item['order_increment_id'], 0, $len);
                    if ($prefix != $this->_helper->getOrderNumberPrefix()) {
                         $item['order_increment_id'] = $this->_helper->getOrderNumberPrefix() . $item['order_increment_id'];
                    }
                } else {
                    $prefix = substr($item['increment_id'], 0, $len);
                    if ($prefix != $this->_helper->getOrderNumberPrefix()) {
                         $item['increment_id'] = $this->_helper->getOrderNumberPrefix() . $item['increment_id'];
                    }
                }
            }
        }

        return $dataSource;
    }
}
