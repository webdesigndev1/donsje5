<?php

namespace WebdesignStudenten\EasyOrderPrefix\Plugin;

class OrderNumber
{
    /**
     * @var \WebdesignStudenten\EasyOrderPrefix\Helper\Data
     */
    protected $_helper;
    protected $request;

    public function __construct(
        \WebdesignStudenten\EasyOrderPrefix\Helper\Data $helper,
        \Magento\Framework\App\Request\Http $request
    ) {
    
        $this->_helper = $helper;
        $this->request = $request;
    }
    
	public function afterGetIncrementId(\Magento\Sales\Model\Order $subject, $result)
	{
        $moduleName = $this->request->getModuleName();
        $controller = $this->request->getControllerName();
        $action     = $this->request->getActionName();
        if ($moduleName == 'checkout' && $controller == 'onepage' && $action == 'success') return;
        $len = strlen($this->_helper->getOrderNumberPrefix());
        $prefix = substr($result, 0, $len);
        if ($prefix != $this->_helper->getOrderNumberPrefix()) {
            return $this->_helper->getOrderNumberPrefix() . $result;
        }
	}

}