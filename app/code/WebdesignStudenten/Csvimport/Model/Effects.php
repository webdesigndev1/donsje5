<?php

namespace WebdesignStudenten\Csvimport\Model;

class Effects
{
    
    /**
    * Retrieve option array
    *
    * @return array
    */
    public static function getOptionArray()
    {
        return [
            'fade'  => __('fade'),
            'blindX'  => __('blindX'),
            'blindY'  => __('blindY'),
            'blindZ'  => __('blindZ'),
            'cover'  => __('cover'),
            'curtainX'  => __('curtainX'),
            'curtainY'  => __('curtainY'),
            'fadeZoom'  => __('fadeZoom'),
            'growX' => __('growX'),
            'growY' => __('growY'),
            'scrollUp' => __('scrollUp'),
            'scrollDown' => __('scrollDown'),
            'scrollLeft' => __('scrollLeft'),
            'scrollRight'=> __('Scroll Right'),
            'scrollHorz'=>__('Scroll Horizontal'),
            'scrollVert'=>__('Scroll Vertical'),
            'shuffle'=>__('Shuffle'),
            'toss'=>__('Toss'),
            'turnUp'=>__('Turn Up'),
            'turnDown'=>__('Turn Down'),
            'turnLeft'=>__('Turn Left'),
            'turnRight'=>__('Turn Right'),
            'uncover'=>__('Uncover'),
            'wipe'=>__('Wipe'),
            'zoom'=>__('Zoom')
        ];
    }

    /**
     * Retrieve option array with empty value
     *
     * @return string[]
     */
    public function getAllOptions()
    {
        $result = [];

        foreach (self::getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }

        return $result;
    }

    /**
     * Retrieve option text by option value
     *
     * @param string $optionId
     * @return string
     */
    public function getOptionText($optionId)
    {
        $options = self::getOptionArray();

        return isset($options[$optionId]) ? $options[$optionId] : null;
    }
}
