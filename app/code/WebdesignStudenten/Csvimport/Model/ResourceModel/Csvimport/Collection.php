<?php
namespace WebdesignStudenten\Csvimport\Model\ResourceModel\Csvimport;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('WebdesignStudenten\Csvimport\Model\Csvimport', 'WebdesignStudenten\Csvimport\Model\ResourceModel\Csvimport');
        $this->_map['fields']['page_id'] = 'main_table.page_id';
    }
}
