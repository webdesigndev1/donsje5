<?php
/**
 * Copyright © 2016 Perception System. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Csvimport edit block
 *
 * @author      Perception System <store@webdesignstudenten.com>
 */
namespace WebdesignStudenten\Csvimport\Block\Adminhtml\Csvimport;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * Initialize csvimport edit block
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'csvimport_id';
        $this->_blockGroup = 'WebdesignStudenten_Csvimport';
        $this->_controller = 'adminhtml_csvimport';

        parent::_construct();

        $this->buttonList->update('save', 'label', __('Upload File'));
        $this->buttonList->add(
            'saveandcontinue',
            [
                'label' => __('Upload and Continue Edit'),
                'class' => 'save',
                'data_attribute' => [
                    'mage-init' => [
                        'button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form'],
                    ],
                ]
            ],
            -100
        );

        $this->buttonList->update('delete', 'label', __('Delete File'));
    }

    /**
     * Retrieve text for header element depending on loaded post
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        if ($this->_coreRegistry->registry('csvimport')->getId()) {
            return __("Edit File '%1'", $this->escapeHtml($this->_coreRegistry->registry('csvimport')->getTitle()));
        } else {
            return __('Upload New File');
        }
    }

    /**
     * Getter of url for "Save and Continue" button
     * tab_id will be replaced by desired by JS later
     *
     * @return string
     */
    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('csvimport/*/save', ['_current' => true, 'back' => 'edit', 'active_tab' => '{{tab_id}}']);
    }

    /**
     * Prepare layout
     *
     * @return \Magento\Framework\View\Element\AbstractBlock
     */
    protected function _prepareLayout()
    {
        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('page_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'content');
                }
            };
        ";
        return parent::_prepareLayout();
    }
}
