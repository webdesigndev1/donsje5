<?php
/**
 * Copyright © 2016 Perception System. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Csvimport edit block
 *
 * @author      Perception System <store@webdesignstudenten.com>
 */
namespace WebdesignStudenten\Csvimport\Block\Adminhtml\Csvimport\Edit;

/**
 * Adminhtml csvimport edit form block
 *
 * @author Perception Magento Core Team <webdesignstudenten.com>
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form',
                'action' => $this->getData('action'),
                'method' => 'post',
                'enctype' => 'multipart/form-data'
                       ]
            ]
        );
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
