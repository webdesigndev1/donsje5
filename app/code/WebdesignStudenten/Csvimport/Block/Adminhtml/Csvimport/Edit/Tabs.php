<?php
/**
 * Copyright © 2016 Perception System. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Csvimport edit block
 *
 * @author      Perception System <store@webdesignstudenten.com>
 */
namespace WebdesignStudenten\Csvimport\Block\Adminhtml\Csvimport\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('csvimport_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Imort File Information'));
    }
}
