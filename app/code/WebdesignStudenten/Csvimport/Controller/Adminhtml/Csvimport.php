<?php

namespace WebdesignStudenten\Csvimport\Controller\Adminhtml;

/**
 * Csvimport Abstract Action
 * @category WebdesignStudenten
 * @package  WebdesignStudenten_Csvimport
 * @module   Csvimport
 * @author   WebdesignStudenten Developer
 */
abstract class Csvimport extends \WebdesignStudenten\Csvimport\Controller\Adminhtml\AbstractAction
{
    const PARAM_CRUD_ID = 'csvimport_id';

    /**
     * Check if admin has permissions to visit related pages.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('WebdesignStudenten_Csvimport::csvimport_csvimports');
    }

    /**
     * Get back result redirect after add/edit.
     *
     * @param \Magento\Framework\Controller\Result\Redirect $resultRedirect
     * @param null                                          $paramCrudId
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    protected function _getBackResultRedirect(
        \Magento\Framework\Controller\Result\Redirect $resultRedirect,
        $paramCrudId = null
    ) {
        switch ($this->getRequest()->getParam('back')) {
            case 'edit':
                $resultRedirect->setPath(
                    '*/*/edit',
                    [
                        static::PARAM_CRUD_ID => $paramCrudId,
                        '_current' => true,
                        'store' => $this->getRequest()->getParam('store'),
                        'saveandclose' => $this->getRequest()->getParam('saveandclose'),
                    ]
                );
                break;
            case 'new':
                $resultRedirect->setPath('*/*/new', ['_current' => true]);
                break;
            default:
                $resultRedirect->setPath('*/*/');
        }

        return $resultRedirect;
    }
}
