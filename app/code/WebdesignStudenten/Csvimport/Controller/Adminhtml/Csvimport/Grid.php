<?php

namespace WebdesignStudenten\Csvimport\Controller\Adminhtml\Csvimport;

/**
 * Csvimport grid action.
 * @category WebdesignStudenten
 * @package  WebdesignStudenten_Csvimport
 * @module   Csvimport
 * @author   WebdesignStudenten Developer
 */
class Grid extends \WebdesignStudenten\Csvimport\Controller\Adminhtml\Csvimport
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        $resultLayout = $this->_resultLayoutFactory->create();

        return $resultLayout;
    }
}
