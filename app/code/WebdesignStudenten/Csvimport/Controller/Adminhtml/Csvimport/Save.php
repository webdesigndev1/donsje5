<?php
namespace WebdesignStudenten\Csvimport\Controller\Adminhtml\Csvimport;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Backend\App\Action;

class Save extends \Magento\Backend\App\Action
{
    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $model = $this->_objectManager->create('WebdesignStudenten\Csvimport\Model\Csvimport');
            if (isset($data['file']['delete'])) {
                $data['file'] = '';
            } else if (isset($data['file']['value'])) {
                $data['file'] = str_replace("webdesign-studenten/import", "", $data['file']['value']);
            } else {
                $uploader = $this->_objectManager->create(
                    '\Magento\MediaStorage\Model\File\Uploader',
                    ['fileId' => 'file']
                );
                $uploader->setAllowedExtensions(['csv']);
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(true);
                $mediaDirectory = $this->_objectManager->get('Magento\Framework\Filesystem')
                        ->getDirectoryRead(DirectoryList::MEDIA);
                $config = $this->_objectManager->get('WebdesignStudenten\Csvimport\Model\Csvimport');
                $result = $uploader->save($mediaDirectory->getAbsolutePath('webdesign-studenten/import'));
                unset($result['tmp_name']);
                unset($result['path']);
                $data['file'] = $result['file'];
            }

            $id = $this->getRequest()->getParam('csvimport_id');
            if ($id) {
                $model->load($id);
            }
            $model->setData($data);
            try {
                $model->save();
                $this->messageManager->addSuccess(__('File Uploaded.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['csvimport_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while Uploading file.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['csvimport_id' => $this->getRequest()->getParam('csvimport_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
