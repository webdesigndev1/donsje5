<?php
namespace WebdesignStudenten\Csvimport\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();
        if (!$installer->tableExists('csvimport')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('csvimport')
            )->addColumn(
                'csvimport_id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'csvimport ID'
            )->addColumn(
                'title',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Title'
            )->addColumn(
                'file',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Image'
            )->addColumn(
                'is_active',
                Table::TYPE_SMALLINT,
                null,
                [],
                'Active Status'
            )->addColumn(
                'csvimport_effects',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Animate In'
            )->addColumn(
                'sort_order',
                Table::TYPE_INTEGER,
                12,
                ['nullable' => false],
                'Sort Order'
            )->addColumn(
                'csvimport_url',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Csvimport URL'
            )->addColumn(
                'csvimport_content',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Csvimport Content'
            )->addColumn(
                'content_position',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Content Position'
            )->addColumn(
                'created_at',
                Table::TYPE_TIMESTAMP,
                null,
                [],
                'Creation Time'
            )->addColumn(
                'update_time',
                Table::TYPE_TIMESTAMP,
                null,
                [],
                'Modification Time'
            )->addIndex(
                $installer->getIdxName('csvimport', ['csvimport_id']),
                ['csvimport_id']
            )->setComment(
                'Csvimport Table'
            );
            $installer->getConnection()->createTable($table);
        }
        $installer->endSetup();

    }
}
