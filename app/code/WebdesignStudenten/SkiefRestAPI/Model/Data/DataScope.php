<?php
/**
 * Admin can sync customer, products, sales, cart, newsletter subscribers, wishlist etc.
 * Copyright (C) 2019  
 * 
 * This file is part of WebdesignStudenten/SkiefRestAPI.
 * 
 * WebdesignStudenten/SkiefRestAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace WebdesignStudenten\SkiefRestAPI\Model\Data;

use WebdesignStudenten\SkiefRestAPI\Api\Data\DataScopeInterface;

class DataScope extends \Magento\Framework\Api\AbstractExtensibleObject implements DataScopeInterface
{

    /**
     * Get datascope_id
     * @return string|null
     */
    public function getDatascopeId()
    {
        return $this->_get(self::DATASCOPE_ID);
    }

    /**
     * Set datascope_id
     * @param string $datascopeId
     * @return \WebdesignStudenten\SkiefRestAPI\Api\Data\DataScopeInterface
     */
    public function setDatascopeId($datascopeId)
    {
        return $this->setData(self::DATASCOPE_ID, $datascopeId);
    }

    /**
     * Get dataScope
     * @return string|null
     */
    public function getDataScope()
    {
        return $this->_get(self::DATASCOPE);
    }

    /**
     * Set dataScope
     * @param string $dataScope
     * @return \WebdesignStudenten\SkiefRestAPI\Api\Data\DataScopeInterface
     */
    public function setDataScope($dataScope)
    {
        return $this->setData(self::DATASCOPE, $dataScope);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \WebdesignStudenten\SkiefRestAPI\Api\Data\DataScopeExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \WebdesignStudenten\SkiefRestAPI\Api\Data\DataScopeExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \WebdesignStudenten\SkiefRestAPI\Api\Data\DataScopeExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}
