<?php
/**
 * Admin can sync customer, products, sales, cart, newsletter subscribers, wishlist etc.
 * Copyright (C) 2019  
 * 
 * This file is part of WebdesignStudenten/SkiefRestAPI.
 * 
 * WebdesignStudenten/SkiefRestAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace WebdesignStudenten\SkiefRestAPI\Model\Data;

use WebdesignStudenten\SkiefRestAPI\Api\Data\UpdateDateInterface;

class UpdateDate extends \Magento\Framework\Api\AbstractExtensibleObject implements UpdateDateInterface
{

    /**
     * Get updatedate_id
     * @return string|null
     */
    public function getUpdatedateId()
    {
        return $this->_get(self::UPDATEDATE_ID);
    }

    /**
     * Set updatedate_id
     * @param string $updatedateId
     * @return \WebdesignStudenten\SkiefRestAPI\Api\Data\UpdateDateInterface
     */
    public function setUpdatedateId($updatedateId)
    {
        return $this->setData(self::UPDATEDATE_ID, $updatedateId);
    }

    /**
     * Get UpdateDate
     * @return string|null
     */
    public function getUpdateDate()
    {
        return $this->_get(self::UPDATEDATE);
    }

    /**
     * Set UpdateDate
     * @param string $updateDate
     * @return \WebdesignStudenten\SkiefRestAPI\Api\Data\UpdateDateInterface
     */
    public function setUpdateDate($updateDate)
    {
        return $this->setData(self::UPDATEDATE, $updateDate);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \WebdesignStudenten\SkiefRestAPI\Api\Data\UpdateDateExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \WebdesignStudenten\SkiefRestAPI\Api\Data\UpdateDateExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \WebdesignStudenten\SkiefRestAPI\Api\Data\UpdateDateExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}
