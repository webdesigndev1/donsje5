<?php
/**
 * Admin can sync customer, products, sales, cart, newsletter subscribers, wishlist etc.
 * Copyright (C) 2019  
 * 
 * This file is part of WebdesignStudenten/SkiefRestAPI.
 * 
 * WebdesignStudenten/SkiefRestAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace WebdesignStudenten\SkiefRestAPI\Model;

use WebdesignStudenten\SkiefRestAPI\Api\DataScopeRepositoryInterface;
use WebdesignStudenten\SkiefRestAPI\Api\Data\DataScopeSearchResultsInterfaceFactory;
use WebdesignStudenten\SkiefRestAPI\Api\Data\DataScopeInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use WebdesignStudenten\SkiefRestAPI\Model\ResourceModel\DataScope as ResourceDataScope;
use WebdesignStudenten\SkiefRestAPI\Model\ResourceModel\DataScope\CollectionFactory as DataScopeCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\ExtensibleDataObjectConverter;

class DataScopeRepository implements DataScopeRepositoryInterface
{

    protected $resource;

    protected $dataScopeFactory;

    protected $dataScopeCollectionFactory;

    protected $searchResultsFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $dataDataScopeFactory;

    protected $extensionAttributesJoinProcessor;

    private $storeManager;

    private $collectionProcessor;

    protected $extensibleDataObjectConverter;

    /**
     * @param ResourceDataScope $resource
     * @param DataScopeFactory $dataScopeFactory
     * @param DataScopeInterfaceFactory $dataDataScopeFactory
     * @param DataScopeCollectionFactory $dataScopeCollectionFactory
     * @param DataScopeSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceDataScope $resource,
        DataScopeFactory $dataScopeFactory,
        DataScopeInterfaceFactory $dataDataScopeFactory,
        DataScopeCollectionFactory $dataScopeCollectionFactory,
        DataScopeSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->dataScopeFactory = $dataScopeFactory;
        $this->dataScopeCollectionFactory = $dataScopeCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataDataScopeFactory = $dataDataScopeFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \WebdesignStudenten\SkiefRestAPI\Api\Data\DataScopeInterface $dataScope
    ) {
        /* if (empty($dataScope->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $dataScope->setStoreId($storeId);
        } */
        
        $dataScopeData = $this->extensibleDataObjectConverter->toNestedArray(
            $dataScope,
            [],
            \WebdesignStudenten\SkiefRestAPI\Api\Data\DataScopeInterface::class
        );
        
        $dataScopeModel = $this->dataScopeFactory->create()->setData($dataScopeData);
        
        try {
            $this->resource->save($dataScopeModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the dataScope: %1',
                $exception->getMessage()
            ));
        }
        return $dataScopeModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getById($dataScopeId)
    {
        $dataScope = $this->dataScopeFactory->create();
        $this->resource->load($dataScope, $dataScopeId);
        if (!$dataScope->getId()) {
            throw new NoSuchEntityException(__('dataScope with id "%1" does not exist.', $dataScopeId));
        }
        return $dataScope->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->dataScopeCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \WebdesignStudenten\SkiefRestAPI\Api\Data\DataScopeInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \WebdesignStudenten\SkiefRestAPI\Api\Data\DataScopeInterface $dataScope
    ) {
        try {
            $dataScopeModel = $this->dataScopeFactory->create();
            $this->resource->load($dataScopeModel, $dataScope->getDatascopeId());
            $this->resource->delete($dataScopeModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the dataScope: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($dataScopeId)
    {
        return $this->delete($this->getById($dataScopeId));
    }
}
