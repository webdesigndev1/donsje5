<?php
/**
 * Admin can sync customer, products, sales, cart, newsletter subscribers, wishlist etc.
 * Copyright (C) 2019  
 * 
 * This file is part of WebdesignStudenten/SkiefRestAPI.
 * 
 * WebdesignStudenten/SkiefRestAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace WebdesignStudenten\SkiefRestAPI\Model;

use WebdesignStudenten\SkiefRestAPI\Api\Data\DataIDInterface;
use WebdesignStudenten\SkiefRestAPI\Api\Data\DataIDInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class DataID extends \Magento\Framework\Model\AbstractModel
{

    protected $dataidDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'webdesignstudenten_skiefrestapi_dataid';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param DataIDInterfaceFactory $dataidDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \WebdesignStudenten\SkiefRestAPI\Model\ResourceModel\DataID $resource
     * @param \WebdesignStudenten\SkiefRestAPI\Model\ResourceModel\DataID\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        DataIDInterfaceFactory $dataidDataFactory,
        DataObjectHelper $dataObjectHelper,
        \WebdesignStudenten\SkiefRestAPI\Model\ResourceModel\DataID $resource,
        \WebdesignStudenten\SkiefRestAPI\Model\ResourceModel\DataID\Collection $resourceCollection,
        array $data = []
    ) {
        $this->dataidDataFactory = $dataidDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve dataid model with dataid data
     * @return DataIDInterface
     */
    public function getDataModel()
    {
        $dataidData = $this->getData();
        
        $dataidDataObject = $this->dataidDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $dataidDataObject,
            $dataidData,
            DataIDInterface::class
        );
        
        return $dataidDataObject;
    }
}
