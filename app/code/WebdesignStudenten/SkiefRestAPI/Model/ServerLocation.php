<?php
/**
 * Admin can sync customer, products, sales, cart, newsletter subscribers, wishlist etc.
 * Copyright (C) 2019  
 * 
 * This file is part of WebdesignStudenten/SkiefRestAPI.
 * 
 * WebdesignStudenten/SkiefRestAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace WebdesignStudenten\SkiefRestAPI\Model;

use WebdesignStudenten\SkiefRestAPI\Api\Data\ServerLocationInterface;
use WebdesignStudenten\SkiefRestAPI\Api\Data\ServerLocationInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class ServerLocation extends \Magento\Framework\Model\AbstractModel
{

    protected $serverlocationDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'webdesignstudenten_skiefrestapi_serverlocation';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ServerLocationInterfaceFactory $serverlocationDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \WebdesignStudenten\SkiefRestAPI\Model\ResourceModel\ServerLocation $resource
     * @param \WebdesignStudenten\SkiefRestAPI\Model\ResourceModel\ServerLocation\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        ServerLocationInterfaceFactory $serverlocationDataFactory,
        DataObjectHelper $dataObjectHelper,
        \WebdesignStudenten\SkiefRestAPI\Model\ResourceModel\ServerLocation $resource,
        \WebdesignStudenten\SkiefRestAPI\Model\ResourceModel\ServerLocation\Collection $resourceCollection,
        array $data = []
    ) {
        $this->serverlocationDataFactory = $serverlocationDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve serverlocation model with serverlocation data
     * @return ServerLocationInterface
     */
    public function getDataModel()
    {
        $serverlocationData = $this->getData();
        
        $serverlocationDataObject = $this->serverlocationDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $serverlocationDataObject,
            $serverlocationData,
            ServerLocationInterface::class
        );
        
        return $serverlocationDataObject;
    }
}
