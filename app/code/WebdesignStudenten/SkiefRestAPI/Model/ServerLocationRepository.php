<?php
/**
 * Admin can sync customer, products, sales, cart, newsletter subscribers, wishlist etc.
 * Copyright (C) 2019  
 * 
 * This file is part of WebdesignStudenten/SkiefRestAPI.
 * 
 * WebdesignStudenten/SkiefRestAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace WebdesignStudenten\SkiefRestAPI\Model;

use WebdesignStudenten\SkiefRestAPI\Api\ServerLocationRepositoryInterface;
use WebdesignStudenten\SkiefRestAPI\Api\Data\ServerLocationSearchResultsInterfaceFactory;
use WebdesignStudenten\SkiefRestAPI\Api\Data\ServerLocationInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use WebdesignStudenten\SkiefRestAPI\Model\ResourceModel\ServerLocation as ResourceServerLocation;
use WebdesignStudenten\SkiefRestAPI\Model\ResourceModel\ServerLocation\CollectionFactory as ServerLocationCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\ExtensibleDataObjectConverter;

class ServerLocationRepository implements ServerLocationRepositoryInterface
{

    protected $resource;

    protected $serverLocationFactory;

    protected $serverLocationCollectionFactory;

    protected $searchResultsFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $dataServerLocationFactory;

    protected $extensionAttributesJoinProcessor;

    private $storeManager;

    private $collectionProcessor;

    protected $extensibleDataObjectConverter;

    /**
     * @param ResourceServerLocation $resource
     * @param ServerLocationFactory $serverLocationFactory
     * @param ServerLocationInterfaceFactory $dataServerLocationFactory
     * @param ServerLocationCollectionFactory $serverLocationCollectionFactory
     * @param ServerLocationSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceServerLocation $resource,
        ServerLocationFactory $serverLocationFactory,
        ServerLocationInterfaceFactory $dataServerLocationFactory,
        ServerLocationCollectionFactory $serverLocationCollectionFactory,
        ServerLocationSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->serverLocationFactory = $serverLocationFactory;
        $this->serverLocationCollectionFactory = $serverLocationCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataServerLocationFactory = $dataServerLocationFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \WebdesignStudenten\SkiefRestAPI\Api\Data\ServerLocationInterface $serverLocation
    ) {
        /* if (empty($serverLocation->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $serverLocation->setStoreId($storeId);
        } */
        
        $serverLocationData = $this->extensibleDataObjectConverter->toNestedArray(
            $serverLocation,
            [],
            \WebdesignStudenten\SkiefRestAPI\Api\Data\ServerLocationInterface::class
        );
        
        $serverLocationModel = $this->serverLocationFactory->create()->setData($serverLocationData);
        
        try {
            $this->resource->save($serverLocationModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the serverLocation: %1',
                $exception->getMessage()
            ));
        }
        return $serverLocationModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getById($serverLocationId)
    {
        $serverLocation = $this->serverLocationFactory->create();
        $this->resource->load($serverLocation, $serverLocationId);
        if (!$serverLocation->getId()) {
            throw new NoSuchEntityException(__('ServerLocation with id "%1" does not exist.', $serverLocationId));
        }
        return $serverLocation->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->serverLocationCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \WebdesignStudenten\SkiefRestAPI\Api\Data\ServerLocationInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \WebdesignStudenten\SkiefRestAPI\Api\Data\ServerLocationInterface $serverLocation
    ) {
        try {
            $serverLocationModel = $this->serverLocationFactory->create();
            $this->resource->load($serverLocationModel, $serverLocation->getServerlocationId());
            $this->resource->delete($serverLocationModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the ServerLocation: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($serverLocationId)
    {
        return $this->delete($this->getById($serverLocationId));
    }
}
