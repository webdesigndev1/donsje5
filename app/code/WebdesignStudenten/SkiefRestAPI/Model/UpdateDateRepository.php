<?php
/**
 * Admin can sync customer, products, sales, cart, newsletter subscribers, wishlist etc.
 * Copyright (C) 2019  
 * 
 * This file is part of WebdesignStudenten/SkiefRestAPI.
 * 
 * WebdesignStudenten/SkiefRestAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace WebdesignStudenten\SkiefRestAPI\Model;

use WebdesignStudenten\SkiefRestAPI\Api\UpdateDateRepositoryInterface;
use WebdesignStudenten\SkiefRestAPI\Api\Data\UpdateDateSearchResultsInterfaceFactory;
use WebdesignStudenten\SkiefRestAPI\Api\Data\UpdateDateInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use WebdesignStudenten\SkiefRestAPI\Model\ResourceModel\UpdateDate as ResourceUpdateDate;
use WebdesignStudenten\SkiefRestAPI\Model\ResourceModel\UpdateDate\CollectionFactory as UpdateDateCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\ExtensibleDataObjectConverter;

class UpdateDateRepository implements UpdateDateRepositoryInterface
{

    protected $resource;

    protected $updateDateFactory;

    protected $updateDateCollectionFactory;

    protected $searchResultsFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $dataUpdateDateFactory;

    protected $extensionAttributesJoinProcessor;

    private $storeManager;

    private $collectionProcessor;

    protected $extensibleDataObjectConverter;

    /**
     * @param ResourceUpdateDate $resource
     * @param UpdateDateFactory $updateDateFactory
     * @param UpdateDateInterfaceFactory $dataUpdateDateFactory
     * @param UpdateDateCollectionFactory $updateDateCollectionFactory
     * @param UpdateDateSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceUpdateDate $resource,
        UpdateDateFactory $updateDateFactory,
        UpdateDateInterfaceFactory $dataUpdateDateFactory,
        UpdateDateCollectionFactory $updateDateCollectionFactory,
        UpdateDateSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->updateDateFactory = $updateDateFactory;
        $this->updateDateCollectionFactory = $updateDateCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataUpdateDateFactory = $dataUpdateDateFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \WebdesignStudenten\SkiefRestAPI\Api\Data\UpdateDateInterface $updateDate
    ) {
        /* if (empty($updateDate->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $updateDate->setStoreId($storeId);
        } */
        
        $updateDateData = $this->extensibleDataObjectConverter->toNestedArray(
            $updateDate,
            [],
            \WebdesignStudenten\SkiefRestAPI\Api\Data\UpdateDateInterface::class
        );
        
        $updateDateModel = $this->updateDateFactory->create()->setData($updateDateData);
        
        try {
            $this->resource->save($updateDateModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the updateDate: %1',
                $exception->getMessage()
            ));
        }
        return $updateDateModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getById($updateDateId)
    {
        $updateDate = $this->updateDateFactory->create();
        $this->resource->load($updateDate, $updateDateId);
        if (!$updateDate->getId()) {
            throw new NoSuchEntityException(__('UpdateDate with id "%1" does not exist.', $updateDateId));
        }
        return $updateDate->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->updateDateCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \WebdesignStudenten\SkiefRestAPI\Api\Data\UpdateDateInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \WebdesignStudenten\SkiefRestAPI\Api\Data\UpdateDateInterface $updateDate
    ) {
        try {
            $updateDateModel = $this->updateDateFactory->create();
            $this->resource->load($updateDateModel, $updateDate->getUpdatedateId());
            $this->resource->delete($updateDateModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the UpdateDate: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($updateDateId)
    {
        return $this->delete($this->getById($updateDateId));
    }
}
