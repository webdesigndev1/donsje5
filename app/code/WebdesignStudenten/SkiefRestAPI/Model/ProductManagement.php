<?php
/**
 * Admin can sync customer, products, sales, cart, newsletter subscribers, wishlist etc.
 * Copyright (C) 2019  
 * 
 * This file is part of WebdesignStudenten/SkiefRestAPI.
 * 
 * WebdesignStudenten/SkiefRestAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace WebdesignStudenten\SkiefRestAPI\Model;

class ProductManagement implements \WebdesignStudenten\SkiefRestAPI\Api\ProductManagementInterface
{

    /**
     * {@inheritdoc}
     */
    public function getNewProducts()
    {
        $customer_access_token = 'uwa2p5bnp9wyiwgk6gkqyq1s50vj3lsq';
        $httpHeaders = new \Zend\Http\Headers();
        $httpHeaders->addHeaders([
            'Authorization' => 'Bearer ' . $customer_access_token,
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ]);

        $request = new \Zend\Http\Request();
        $request->setHeaders($httpHeaders);
        $request->setUri('http://soumik.websitetestserver.eu/rest/V1/products?fields=items[sku,name]&searchCriteria[pageSize]=1000');
        
        // Page 1 to 205
//        $request->setUri('http://soumik.websitetestserver.eu/rest/V1/products?searchCriteria[pageSize]=10&searchCriteria[currentPage]=1');
//        $request->setUri('http://soumik.websitetestserver.eu/rest/V1/products?searchCriteria[pageSize]=10&searchCriteria[currentPage]=205');

        //Get product info by ID
//        $prodID = '555';
//        $request->setUri('http://soumik.websitetestserver.eu/rest/V1/products?searchCriteria[filterGroups][0][filters][0][field]=entity_id&searchCriteria[filterGroups][0][filters][0][condition_type]=eq&searchCriteria[filterGroups][0][filters][0][value]=' . $prodID);
        $client = new \Zend\Http\Client();

        $response = $client->send($request);
        return $response->getBody();
//        $products = json_decode($response->getBody());
//        
//        foreach ($products->items as $key => $product) {
//            printf("Product SKU: %s, product name: %s\n", $product->sku, $product->name);
//        }
    }
}
