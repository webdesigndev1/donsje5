<?php
/**
 * Admin can sync customer, products, sales, cart, newsletter subscribers, wishlist etc.
 * Copyright (C) 2019  
 * 
 * This file is part of WebdesignStudenten/SkiefRestAPI.
 * 
 * WebdesignStudenten/SkiefRestAPI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace WebdesignStudenten\SkiefRestAPI\Api\Data;

interface ServerLocationInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const SERVERLOCATION_ID = 'serverlocation_id';
    const SERVERLOCATION = 'ServerLocation';

    /**
     * Get serverlocation_id
     * @return string|null
     */
    public function getServerlocationId();

    /**
     * Set serverlocation_id
     * @param string $serverlocationId
     * @return \WebdesignStudenten\SkiefRestAPI\Api\Data\ServerLocationInterface
     */
    public function setServerlocationId($serverlocationId);

    /**
     * Get ServerLocation
     * @return string|null
     */
    public function getServerLocation();

    /**
     * Set ServerLocation
     * @param string $serverLocation
     * @return \WebdesignStudenten\SkiefRestAPI\Api\Data\ServerLocationInterface
     */
    public function setServerLocation($serverLocation);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \WebdesignStudenten\SkiefRestAPI\Api\Data\ServerLocationExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \WebdesignStudenten\SkiefRestAPI\Api\Data\ServerLocationExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \WebdesignStudenten\SkiefRestAPI\Api\Data\ServerLocationExtensionInterface $extensionAttributes
    );
}
