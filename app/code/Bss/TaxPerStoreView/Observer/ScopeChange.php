<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * BSS Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BSS Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category  BSS
 * @package   Bss_TaxPerStoreView
 * @author    Extension Team
 * @copyright Copyright (c) 2016-2017 BSS Commerce Co. ( http://bsscommerce.com )
 * @license   http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\TaxPerStoreView\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Bss\TaxPerStoreView\Helper\Data;

class ScopeChange implements ObserverInterface
{
    protected $helper;

    public function __construct(
        Data $helper,
        \Magento\Framework\Registry $registry
    ) {

        $this->helper = $helper;
        $this->registry = $registry;
    }

    private function getAttributeObject()
    {
        return $this->registry->registry('entity_attribute');
    }

    public function execute(EventObserver $observer)
    {
        if (!$this->helper->isEnable()) {
            return $this;
        }
        $attributeObject = $this->getAttributeObject()->getAttributeCode();

        $form = $observer->getForm();
        $fieldset = $form->getElement('advanced_fieldset');
        $fieldset->removeField('is_global');

        $scopes = [
            \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE => __('Store View'),
            \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE => __('Website'),
            \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL => __('Global'),
        ];
        if ($attributeObject== 'status') {
            unset($scopes[\Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE]);
        }

        $fieldset->addField(
            'is_global',
            'select',
            [
                'name' => 'is_global',
                'label' => __('Scope'),
                'title' => __('Scope'),
                'note' => __('Declare attribute value saving scope.'),
                'values' => $scopes
            ],
                'attribute_code'
        );
    }
}
