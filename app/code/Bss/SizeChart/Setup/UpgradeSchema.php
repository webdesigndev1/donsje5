<?php
namespace Bss\SizeChart\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $sizeChartTable = $installer->getTable('bss_size_chart');
            if ($installer->tableExists($sizeChartTable)) {
                $installer->getConnection()->addColumn(
                    $sizeChartTable,
                    'override_product_setting',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'nullable' => false,
                        'comment' => 'Override Product Setting'
                    ]
                );
            }
            $ruleTable = $installer->getTable('bss_rule');
            if (!$installer->tableExists($ruleTable)) {
                $table = $installer->getConnection()->newTable(
                    $ruleTable
                )->addColumn(
                    'rule_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'Rule ID'
                )->addColumn(
                    'conditions_serialized',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    '2M',
                    [],
                    'Conditions Serialized'
                )->addIndex(
                    $installer->getIdxName('bss_rule', ['rule_id']),
                    ['rule_id']
                )->setComment('Bss Rules Table');
                $installer->getConnection()->createTable($table);
            }
            $sizeChartRuleTable = $installer->getTable('bss_sc_rule');
            if (!$installer->tableExists($sizeChartRuleTable)) {
                $table = $installer->getConnection()->newTable($sizeChartRuleTable)->addColumn(
                    'rule_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'Rule Id'
                )->addColumn(
                    'size_chart_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    '2M',
                    [],
                    'Size Chart Id'
                )->addIndex($installer->getIdxName('bss_sc_rule', ['rule_id']), ['rule_id'])->setComment(
                    'Bss Size Chart Rules Table'
                );
                $installer->getConnection()->createTable($table);
            }
        }
        $productSizeChartTable = $installer->getTable('bss_product_sizechart');
        if (!$installer->tableExists($productSizeChartTable)) {
            $table = $installer->getConnection()->newTable(
                $productSizeChartTable
            )->addColumn(
                'size_chart_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false],
                'Size Chart Id'
            )->addColumn(
                'product_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '2M',
                [],
                'Product Id'
            )->addIndex(
                $installer->getIdxName('bss_product_sizechart', ['size_chart_id']),
                ['size_chart_id']
            )->setComment('Bss Size Chart Products Table');
            $installer->getConnection()->createTable($table);
        }
        if (version_compare($context->getVersion(), '1.0.8', '<')) {
            $sizeChartProductTable = $installer->getTable('bss_product_sizechart');
            if ($installer->tableExists($sizeChartProductTable)) {
                $installer->getConnection()->addColumn(
                    $sizeChartProductTable,
                    'overwrite',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                        'nullable' => false,
                        'unsigned' => true,
                        'default' => '0',
                        'comment' => 'Overwrite Product Rule'
                    ]
                );
            }
        }
        $installer->endSetup();
    }
}