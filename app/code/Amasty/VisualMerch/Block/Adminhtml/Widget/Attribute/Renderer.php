<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_VisualMerch
 */


namespace Amasty\VisualMerch\Block\Adminhtml\Widget\Attribute;

/**
 * @method string getLabel()
 * @method string getValue()
 * @method string getCode()
 */
class Renderer extends \Magento\Framework\DataObject
{
    /**
     * @var \Magento\Framework\Escaper
     */
    public $escaper;

    /**
     * @param \Magento\Framework\Escaper $escaper
     * @param array $data
     */
    public function __construct(\Magento\Framework\Escaper $escaper, array $data = [])
    {
        parent::__construct($data);
        $this->escaper = $escaper;
    }

    /**
     * @param bool $useTitle
     * @return string
     */
    public function render($useTitle = true)
    {
        return '<div class="' . $this->getCode() . ' ' . $this->getCode() . '-' . $this->getStyleValue() . '">'
            . ($useTitle ? $this->escaper->escapeHtml($this->getLabel()) . ': ' : '')
            . $this->getValue()
            . '</div>';
    }

    /**
     * @return string
     */
    public function getStyleValue()
    {
        return strip_tags($this->getData('value'));
    }
}
