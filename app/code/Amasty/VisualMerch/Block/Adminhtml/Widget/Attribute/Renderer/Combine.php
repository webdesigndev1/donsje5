<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_VisualMerch
 */


namespace Amasty\VisualMerch\Block\Adminhtml\Widget\Attribute\Renderer;

class Combine extends \Amasty\VisualMerch\Block\Adminhtml\Widget\Attribute\Renderer
{
    /**
     * @param bool $useTitle
     * @return string
     */
    public function render($useTitle = true)
    {
        $output = '<div class="combine-attribute">';
        foreach ($this->getData() as $renderer) {
            /**
             * @var \Amasty\VisualMerch\Block\Adminhtml\Widget\Attribute\Renderer $renderer
             */
            $output .= $renderer->render(false);
        }
        $output .= "</div>";

        return $output;
    }
}
