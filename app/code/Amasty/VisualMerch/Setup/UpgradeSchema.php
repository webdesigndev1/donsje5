<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_VisualMerch
 */


namespace Amasty\VisualMerch\Setup;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @var Operation\UpdateIndexerColumn
     */
    private $updateIndexerColumn;

    /**
     * @var Operation\RemoveStoreColumn
     */
    private $removeStoreColumn;

    public function __construct(
        Operation\UpdateIndexerColumn $updateIndexerColumn,
        Operation\RemoveStoreColumn $removeStoreColumn
    ) {
        $this->updateIndexerColumn = $updateIndexerColumn;
        $this->removeStoreColumn = $removeStoreColumn;
    }

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws LocalizedException
     * @throws \Zend_Db_Exception
     * @throws \Zend_Validate_Exception
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.2.0', '<')) {
            $this->createExdenedEavIndexTable($setup);
        }
        if (version_compare($context->getVersion(), '1.3.7', '<')) {
            $this->updateIndexerColumn->execute($setup);
        }
        if (version_compare($context->getVersion(), '1.3.8', '<')) {
            $this->removeStoreColumn->execute($setup);
        }

        $setup->endSetup();
    }

    /**
     * @param SchemaSetupInterface $setup
     * @throws \Zend_Db_Exception
     */
    public function createExdenedEavIndexTable(SchemaSetupInterface $setup)
    {
        $tableName = $setup->getTable('amasty_merchandiser_product_index_eav');
        $table = $setup->getConnection()
            ->newTable($tableName)
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false, 'primary' => true],
                'Entity ID'
            )
            ->addColumn(
                'attribute_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['unsigned' => true, 'nullable' => false, 'primary' => true],
                'Attribute ID'
            )
            ->addColumn(
                'store_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                null,
                ['unsigned' => true, 'nullable' => false, 'primary' => true],
                'Store ID'
            )
            ->addColumn(
                'value',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false, 'primary' => true],
                'Value'
            )
            ->addColumn(
                'source_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false, 'primary' => true],
                'Source Id'
            )
            ->addIndex(
                $setup->getIdxName('amasty_merchandiser_product_index_eav', ['attribute_id']),
                ['attribute_id']
            )
            ->addIndex(
                $setup->getIdxName('amasty_merchandiser_product_index_eav', ['store_id']),
                ['store_id']
            )
            ->addIndex(
                $setup->getIdxName('amasty_merchandiser_product_index_eav', ['value']),
                ['value']
            )
            ->setComment(
                'Amasty Product EAV Index Table For Merchandiser'
            );
        if (!$setup->getConnection()->isTableExists($tableName)) {
            $setup->getConnection()->createTable($table);
        }
    }
}
